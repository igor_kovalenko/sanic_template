from schematics.models import Model as Model
from schematics.types import IntType, StringType
from sanic_transmute import APIException


def validate_nickname(value):
    if value[0].upper() != value[0]:
        raise APIException('The nickname must begin with a capital letter!', code=404)
    return value


class User(Model):
    id = IntType(required=False, serialize_when_none=False)
    nickname = StringType(required=True, validators=[validate_nickname])
