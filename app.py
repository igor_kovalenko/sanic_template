from sanic import Sanic
from sanic_transmute import add_swagger, add_swagger_api_route
from gino.ext.sanic import Gino
from blueprints import init_bp
from settings import Settings

# Инициализируем объект прилоежния
app = Sanic(__name__)

# Добавляем в объект приложения настройки
app.config.from_object(Settings)

# Инициализируем СУБД
db = Gino()
db.init_app(app)

# Инициализируем blueprint
init_bp(app)

# Инициализируем swagger
add_swagger(app, json_route="/api/v1/swagger.json", html_route="/api/v1/swagger")
