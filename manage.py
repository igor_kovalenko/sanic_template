#!/usr/bin/python3

from app import app
from sanic_script import Manager
# from alembic_script import ManageMigrations


manager = Manager(app)
# manager.add_command("migrate", ManageMigrations())


@manager.command
def run():
    app.run(host="0.0.0.0", port=8000)


if __name__ == "__main__":
    manager.run()
