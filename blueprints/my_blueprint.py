from sanic.response import json
from sanic.exceptions import abort
from sanic import Blueprint
from sanic_transmute import describe, add_route
from models import User
from schematics_model.user import User as UserSerializer


bp = Blueprint('my_blueprint')


@describe(paths='/users/', methods='GET')
async def get_users(request) -> [UserSerializer]:
    all_users = await User.query.gino.all()
    return [{'id': user.id, 'nickname': user.nickname} for user in all_users]


@describe(paths='/users/new/', methods='POST', body_parameters=['nickname', ])
async def create_new_user(request, nickname: str) -> UserSerializer:

    # Сериализуем и валидируем полученные значения
    serializer = UserSerializer({'nickname': nickname}, validate=True)
    user = await User.create(**serializer.to_native())
    return UserSerializer({
        'id': user.id,
        'nickname': user.nickname
    })


@describe(paths='/users/<user_id>', methods='GET', parameter_descriptions={'user_id': 'ID пользователя'})
async def get_user(request, user_id) -> UserSerializer:

    if not user_id.isdigit():
        abort(400, 'invalid user id')
    user = await User.get_or_404(int(user_id))
    return json({'name': user.nickname})

add_route(bp, get_users)
add_route(bp, create_new_user)
add_route(bp, get_user)
